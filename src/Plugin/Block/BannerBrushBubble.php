<?php

namespace Drupal\cattask_alpha_theme_support\Plugin\Block;

use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'BannerBrushBubble' block.
 *
 * @Block(
 *  id = "banner_brush_bubble",
 *  admin_label = @Translation("Banner brush bubble"),
 * )
 */
class BannerBrushBubble extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['title'],
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['sub_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub title'),
      '#default_value' => $this->configuration['sub_title'],
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#default_value' => $this->configuration['button_text'],
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['button_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Button url'),
      '#default_value' => $this->configuration['button_url'],
      '#weight' => '0',
    ];
    $max_filesize = min(Bytes::toNumber('5 MB'), Environment::getUploadMaxSize());

    $form['bubble_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Bubble image'),
      '#default_value' => [$this->configuration['bubble_image']],
      '#upload_location' => 'public://banner_brush_bubble',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [$max_filesize],
        'file_validate_image_resolution' => ['723x688'],
      ],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['title'] = $form_state->getValue('title');
    $this->configuration['sub_title'] = $form_state->getValue('sub_title');
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['button_url'] = $form_state->getValue('button_url');
    if ($form_state->hasValue('bubble_image') &&
      isset($form_state->getValue('bubble_image')[0])) {
      $this->configuration['bubble_image'] = $form_state->getValue('bubble_image')[0];
      $file = File::load($this->configuration['bubble_image']);
      // Add to file usage calculation.
      \Drupal::service('file.usage')->add($file, 'cattask_alpha_theme_support', 'file', $file->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'banner_brush_bubble';
    $build['#content']['title'] = $this->configuration['title'];
    $build['#content']['sub_title'] = $this->configuration['sub_title'];
    $build['#content']['button_text'] = $this->configuration['button_text'];
    $build['#content']['button_url'] = $this->configuration['button_url'];
    $build['#content']['bubble_image'] = '';
    if (!empty($this->configuration['bubble_image'])) {
      $file = File::load($this->configuration['bubble_image']);
      if ($file instanceof File) {
        $build['#content']['bubble_image'] = file_create_url($file->uri->value);
      }
    }

    return $build;
  }

}
