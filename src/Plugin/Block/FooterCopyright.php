<?php

namespace Drupal\cattask_alpha_theme_support\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'FooterCopyright' block.
 *
 * @Block(
 *  id = "footer_copyright",
 *  admin_label = @Translation("Footer copyright"),
 * )
 */
class FooterCopyright extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'content' => [
        'value' => '',
        'format' => 'full_html',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    if (!is_array($this->configuration['content'])) {
      $this->configuration['content'] = [
        'value' => '',
        'format' => 'full_html',
      ];
    }
    $form['content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Content'),
      '#default_value' => $this->configuration['content']['value'],
      '#format'=> $this->configuration['content']['format'],
      '#weight' => '10',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['content'] = $form_state->getValue('content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'footer_copyright';
    $build['#content'] = $this->configuration['content'];

    return $build;
  }

}
