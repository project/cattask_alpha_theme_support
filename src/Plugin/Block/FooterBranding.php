<?php

namespace Drupal\cattask_alpha_theme_support\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Plugin\Block\SystemBrandingBlock;

/**
 * Provides a 'FooterBranding' block.
 *
 * @Block(
 *  id = "footer_branding",
 *  admin_label = @Translation("Footer branding"),
 * )
 */
class FooterBranding extends SystemBrandingBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'text' => ''
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#default_value' => $this->configuration['text'],
      '#weight' => '10',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['text'] = $form_state->getValue('text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $parent_build = parent::build();
    $build['#theme'] = 'footer_branding';
    $build['#content'] = $parent_build;
    $build['#content']['text'] = ['#markup' => $this->configuration['text']];

    return $build;
  }

}
