<?php

namespace Drupal\cattask_alpha_theme_support\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'CustomizedServices' block.
 *
 * @Block(
 *  id = "customized_services",
 *  admin_label = @Translation("Customized services"),
 * )
 */
class CustomizedServices extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'customized_services';
    $build['#content'][] = isset($this->configuration['text']) ?? $this->configuration['text'];
    return $build;
  }

}
