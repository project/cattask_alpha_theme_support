<?php

namespace Drupal\cattask_alpha_theme_support\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simplenews\Plugin\Block\SimplenewsSubscriptionBlock;

/**
 * Provides a 'FooterSubscribe' block.
 *
 * @Block(
 *  id = "footer_subscribe",
 *  admin_label = @Translation("Footer subscribe"),
 * )
 */
class FooterSubscribe extends SimplenewsSubscriptionBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'social_networks' => []
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Display exist networks
    $wrapper_id = Html::getUniqueId('add-more-networks-wrapper');

    $form['social_networks'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Social networks'),
      '#attributes' => [
        'id' => $wrapper_id,
      ],
      //'#id' => $wrapper_id,
    ];

    $form['social_networks']['list'] = [
      '#type' => 'table',
      '#caption' => $this->t('Network list'),
      '#header' => [
        $this->t('Name'),
        $this->t('Icon'),
        $this->t('Link'),
        $this->t('Operations'),
      ]
    ];

    foreach ($this->configuration['social_networks'] as $i => $network) {
      $form['social_networks']['list'][$i] = [
        [
          '#plain_text' => $network['name'],
        ],
        [
          '#plain_text' => $network['icon'],
        ],
        [
          '#plain_text' => $network['link'],
        ],
        [
          '#type' => 'button',
          '#value' => $this->t('Remove'),
          '#remove_btn' => TRUE,
          '#data_index' => $i,
          '#name' => 'remove_btn_' . $i,
          '#ajax' => [
            'callback' => [get_class($this), 'addMoreAjax'],
            'wrapper' => $wrapper_id,
            'effect' => 'fade',
          ]],
      ];
    }

    $form['social_networks']['add_network']['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];
    $form['social_networks']['add_network']['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      //'#required' => TRUE,
    ];
    $form['social_networks']['add_network']['icon'] = [
      '#type' => 'textfield',
      '#title' => t('Icon'),
      //'#required' => TRUE,
    ];

    $form['social_networks']['add_network']['link'] = [
      '#type' => 'url',
      '#title' => t('Link'),
      //'#required' => TRUE,
    ];

    $form['social_networks']['add_network']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Add network'),
      '#add_network_btn' => TRUE,
      '#ajax' => [
        'callback' => [get_class($this), 'addMoreAjax'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
      ]
    ];

    return $form;
  }

  public static function addMoreAjax($form, FormStateInterface $form_state) {
    $trigger_element = $form_state->getTriggeringElement();
    $parents = $trigger_element['#parents'];
    if ($trigger_element) {
      if ($trigger_element['#add_network_btn']) {
        array_pop($parents);
        array_pop($parents);
      }
      if ($trigger_element['#remove_btn']) {
        array_pop($parents);
        array_pop($parents);
        array_pop($parents);
      }
    }
    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $trigger_element = $form_state->getTriggeringElement();
    if ($trigger_element && isset($trigger_element['#add_network_btn'])) {
      isset($this->configuration['social_networks']) ?? $this->configuration['social_networks'] = [];
      $data = $form_state->getValues();
      $this->configuration['social_networks'][] = [
        'name' => $data['social_networks']['add_network']['name'],
        'icon' => $data['social_networks']['add_network']['icon'],
        'link' => $data['social_networks']['add_network']['link'],
      ];
    }

    if ($trigger_element &&
      isset($trigger_element['#remove_btn']) &&
      isset($this->configuration['social_networks']) &&
      is_array($this->configuration['social_networks'])) {
      $new_data = [];
      foreach ($this->configuration['social_networks'] as $i => $network) {
        if ($i !== (int) $trigger_element['#data_index']) {
          $new_data[] = $network;
        }
      }
      $this->configuration['social_networks'] = $new_data;
    }
    parent::blockValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'footer_subscribe';
    $build['#social_networks'] = $this->configuration['social_networks'];
    $form = parent::build();
    $form['message']['#markup'] = '<p>' . $form['message']['#markup'] . '</p>';
    $form['message']['#weight'] = -99;
    $build['#subscription_form'] = $form;

    return $build;
  }

}
