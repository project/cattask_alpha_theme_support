<?php

namespace Drupal\cattask_alpha_theme_support\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;

/**
 * Provides a 'FooterGallery' block.
 *
 * @Block(
 *  id = "footer_gallery",
 *  admin_label = @Translation("Footer gallery"),
 * )
 */
class FooterGallery extends BlockBase {

  const LIST_TYPE_BLOG_POST = 'blog_post';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list_type' => static::LIST_TYPE_BLOG_POST,
      'list_length' => 9,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['list_type'] = [
      '#type' => 'select',
      '#title' => $this->t('What to show'),
      '#default_value' => $this->configuration['list_type'],
      '#options' => [
        static::LIST_TYPE_BLOG_POST => $this->t('Blog posts'),
      ],
      '#weight' => '10',
    ];
    $form['list_length'] = [
      '#type' => 'number',
      '#title' => $this->t('How many to show'),
      '#default_value' => $this->configuration['list_length'],
      '#weight' => '10',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['list_type'] = $form_state->getValue('list_type');
    $this->configuration['list_length'] = $form_state->getValue('list_length');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'footer_gallery';

    // Load the newest blog posts.
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $ids = $storage->getQuery()
      ->condition('type', 'blog_post')
      ->sort('created', 'DESC')
      ->range(0, $this->configuration['list_length'])
      ->execute();
    $blog_posts = Node::loadMultiple($ids);
    $items = [];
    foreach ($blog_posts as $blog_post) {
      $blog_post = $this->current_language_version($blog_post);
      $files = $blog_post->get('field_cover')->referencedEntities();
      $image_uri = '';
      $image_uri_paragraph_article_item = '';
      if (count($files)) {
        $image_uri = $files[0]->uri[0]->value;
        // Load image style "paragraph_article_item".
        $style = ImageStyle::load('paragraph_article_item');
        // Get URI.
        $image_uri_paragraph_article_item = $style->buildUri($image_uri);
        $style->createDerivative($image_uri, $image_uri_paragraph_article_item);
      }
      $cover_url = file_create_url($image_uri);
      $cover_url_paragraph_article_item = file_create_url($image_uri_paragraph_article_item);

      $items[] = [
        'label' => $blog_post->label(),
        'image' => $cover_url_paragraph_article_item,
        'url' => $blog_post->toUrl(),
      ];
    }
    $build['#content'] = $items;

    return $build;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  function current_language_version(\Drupal\Core\Entity\EntityInterface $entity) {
    $language = $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($entity->hasTranslation($language)) {
      $entity = $entity->getTranslation($language);
    }

    return $entity;
  }
}
